import { Component, OnInit } from '@angular/core';

import { CaregiverService } from '../shared/caregiver.service'
import { Caregiver } from '../shared/caregiver.model'

@Component({
  selector: 'app-caregiver-list',
  templateUrl: './caregiver-list.component.html',
  styleUrls: ['./caregiver-list.component.css']
})
export class CaregiverListComponent implements OnInit {

  constructor(private caregiverService : CaregiverService) { }

  ngOnInit() {
    this.caregiverService.getAllCaregivers();
  }

  editCaregiver(caregiver : Caregiver){
    this.caregiverService.selectedCaregiver = Object.assign({}, caregiver);
  }

  onDelete(id : number){
    this.caregiverService.deleteCaregiver(id)
      .subscribe(data => {
        this.caregiverService.getAllCaregivers();
        window.location.reload();
      })
  }

}
