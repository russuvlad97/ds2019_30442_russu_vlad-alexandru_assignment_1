export class Patient {
    id: number
    name: string
    username: string
    password: string
    birth_date: string
    gender: string
    address: string
    medicalRecord: string
}
