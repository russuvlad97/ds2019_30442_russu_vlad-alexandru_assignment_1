import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, RequestMethod } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

import {Patient} from './patient.model'

@Injectable()
export class PatientService {

  selectedPatient : Patient;
  patientList : Patient[];
  constructor(private http : Http) { }

  getAllPatients(){
    this.http.get('http://localhost:8080/doctor/patients')
      .map((data : Response) => {
        return data.json() as Patient[];
      })
      .toPromise()
      .then(x => {
        this.patientList = x;
      })
  }

  postPatient(patient : Patient){
    var body = JSON.stringify(patient);
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post, headers : headerOptions});
    return this.http.post('http://localhost:8080/doctor/patient', body, requestOptions).map(x=>x.json());
  }

  putPatient(id, patient : Patient){
    var body = JSON.stringify(patient);
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Put, headers : headerOptions});
    return this.http.put('http://localhost:8080/doctor/patient/' + id , body, requestOptions).map(x=>x.json());
  }

  deletePatient(id : number){
    return this.http.delete('http://localhost:8080/doctor/patient/' + id); 
    // window.location.reload();
  }

}
