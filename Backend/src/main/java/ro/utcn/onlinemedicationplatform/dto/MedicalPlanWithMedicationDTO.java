package ro.utcn.onlinemedicationplatform.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class MedicalPlanWithMedicationDTO {

    private Long id;
    private String name;
    private String instructions;
    private String treatmentPeriod;
    private List<MedicationDTO> medicationDTOList;

}
