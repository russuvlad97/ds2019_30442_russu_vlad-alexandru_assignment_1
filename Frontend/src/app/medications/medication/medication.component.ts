import { Component, OnInit } from '@angular/core';

import { MedicationService  } from '../shared/medication.service'
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-medication',
  templateUrl: './medication.component.html',
  styleUrls: ['./medication.component.css']
})
export class MedicationComponent implements OnInit {

  constructor(private medicationService : MedicationService) { }

  ngOnInit() {
    this.resetForm();
  }

  resetForm(form? : NgForm) {
    if(form != null)
      form.reset();
    this.medicationService.selectedMedication = {
      id: null,
      name: "",
      sideEffects: "",
      dosage: ""
    }
  }

  onSubmit(form : NgForm){
    if(form.value.id == null){
      this.medicationService.postMedication(form.value)
        .subscribe(data => {
          this.resetForm(form);
          this.medicationService.getAllMedications();
        })
    }
    else{
      this.medicationService.putMedication(form.value.id, form.value)
        .subscribe(data => {
          this.resetForm(form);
          this.medicationService.getAllMedications();
        })
    }
  }

}
