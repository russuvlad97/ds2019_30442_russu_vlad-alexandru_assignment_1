import { Component, OnInit } from '@angular/core';
import { MedicationService } from './shared/medication.service';

@Component({
  selector: 'app-medications',
  templateUrl: './medications.component.html',
  styleUrls: ['./medications.component.css']
})
export class MedicationsComponent implements OnInit {

  constructor(private medicationService: MedicationService) { }

  ngOnInit() {
  }

}
