package ro.utcn.onlinemedicationplatform;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import ro.utcn.onlinemedicationplatform.dto.MedicationDTO;
import ro.utcn.onlinemedicationplatform.entities.Medication;

import java.util.TimeZone;

@SpringBootApplication
public class OnlineMedicationApplication {

    public static void main(String[] args) {
        TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
        SpringApplication.run(OnlineMedicationApplication.class, args);
    }

}