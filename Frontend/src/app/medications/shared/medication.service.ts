import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, RequestMethod } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

import {Medication} from './medication.model'

@Injectable()
export class MedicationService {

  selectedMedication : Medication;
  medicationList : Medication[];
  constructor(private http : Http) { }

  getAllMedications(){
    this.http.get('http://localhost:8080/doctor/medications')
      .map((data : Response) => {
        return data.json() as Medication[];
      })
      .toPromise()
      .then(x => {
        this.medicationList = x;
      })
  }

  postMedication(medication : Medication){
    var body = JSON.stringify(medication);
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post, headers : headerOptions});
    return this.http.post('http://localhost:8080/doctor/medication', body, requestOptions).map(x=>x.json());
  }

  putMedication(id, medication : Medication){
    var body = JSON.stringify(medication);
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Put, headers : headerOptions});
    return this.http.put('http://localhost:8080/doctor/medication/' + id , body, requestOptions).map(x=>x.json());

  }

  deleteMedication(id : number){
    return this.http.delete('http://localhost:8080/doctor/medication/' + id); 
    // window.location.reload();
  }

}
