package ro.utcn.onlinemedicationplatform.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.utcn.onlinemedicationplatform.dto.MedicationDTO;
import ro.utcn.onlinemedicationplatform.dto.builders.MedicationBuilder;
import ro.utcn.onlinemedicationplatform.dto.builders.MedicationViewBuilder;
import ro.utcn.onlinemedicationplatform.dto.view.MedicationViewDTO;
import ro.utcn.onlinemedicationplatform.entities.Medication;
import ro.utcn.onlinemedicationplatform.errorhandler.ResourceNotFoundException;
import ro.utcn.onlinemedicationplatform.repositories.MedicationRepository;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class MedicationService {

    private final MedicationRepository medicationRepository;

    @Autowired
    public MedicationService(MedicationRepository medicationRepository) {
        this.medicationRepository = medicationRepository;
    }

    public MedicationViewDTO findMedicationById(Long id){
        Optional<Medication> medication = medicationRepository.findById(id);
        if(!medication.isPresent()){
            throw new ResourceNotFoundException("Medication", "medication_id", id);
        }
        return MedicationViewBuilder.generateDTOFromEntity(medication.get());
    }

    public List<MedicationViewDTO> findAllMedications(){
        List<Medication> medications = medicationRepository.findAll();
        return medications.stream()
                .map(MedicationViewBuilder::generateDTOFromEntity)
                .collect(Collectors.toList());
    }

    public Long insert(MedicationDTO medicationDTO){
        //TODO validator

        return medicationRepository
                .save(MedicationBuilder.generateEntityFromDTO(medicationDTO))
                .getId();
    }

    public Long update(Long id, MedicationDTO medicationDTO){
        Optional<Medication> medication = medicationRepository.findById(id);
        if(!medication.isPresent()){
            throw new ResourceNotFoundException("Medication", "medication_id", id);
        }
        this.medicationRepository.deleteById(id);
        //TODO validator
        return medicationRepository
                .save(MedicationBuilder.generateEntityFromDTO(medicationDTO))
                .getId();
    }

    public void delete(Long id) {
        this.medicationRepository.deleteById(id);
    }
}
