package ro.utcn.onlinemedicationplatform.services;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ro.utcn.onlinemedicationplatform.dto.CaregiverDTO;
import ro.utcn.onlinemedicationplatform.dto.builders.CaregiverBuilder;
import ro.utcn.onlinemedicationplatform.dto.builders.CaregiverViewBuilder;
import ro.utcn.onlinemedicationplatform.dto.view.CaregiverViewDTO;
import ro.utcn.onlinemedicationplatform.entities.Caregiver;
import ro.utcn.onlinemedicationplatform.errorhandler.ResourceNotFoundException;
import ro.utcn.onlinemedicationplatform.repositories.CaregiverRepository;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class CaregiverService {

    private final CaregiverRepository caregiverRepository;

    @Autowired
    public CaregiverService(CaregiverRepository caregiverRepository) {
        this.caregiverRepository = caregiverRepository;
    }

    public CaregiverDTO findCaregiverById(Long id){
        Optional<Caregiver> caregiver = caregiverRepository.findById(id);
        if(!caregiver.isPresent()){
            throw new ResourceNotFoundException("Caregiver", "caregiver_id", id);
        }
        return CaregiverBuilder.generateDTOFromEntity(caregiver.get());
    }

    public List<CaregiverDTO> findAllCaregivers(){
        List<Caregiver> caregivers = caregiverRepository.findAll();
        return caregivers.stream()
                .map(CaregiverBuilder::generateDTOFromEntity)
                .collect(Collectors.toList());
    }

    public Long insert(CaregiverDTO caregiverDTO){
        //TODO validation
        return caregiverRepository
                .save(CaregiverBuilder.generateEntityFromDTO(caregiverDTO))
                .getId();
    }

    public Long update(Long id, CaregiverDTO caregiverDTO){
        Optional<Caregiver> caregiver = caregiverRepository.findById(id);
        if(!caregiver.isPresent()){
            throw new ResourceNotFoundException("Caregiver", "caregiver_id", id);
        }
        //TODO validator
        this.caregiverRepository.deleteById(id);
        return caregiverRepository
                .save(CaregiverBuilder.generateEntityFromDTO(caregiverDTO))
                .getId();
    }

    public void delete(Long id) {
        this.caregiverRepository.deleteById(id);
    }

    public Long login(String username, String password){
        Caregiver caregiver = caregiverRepository.findByUsername(username).get();
        if(caregiver != null){
            if(caregiver.getPassword().equals(password)){
                return caregiver.getId();
            }
        }
        return Long.parseLong("-1");
    }
}

