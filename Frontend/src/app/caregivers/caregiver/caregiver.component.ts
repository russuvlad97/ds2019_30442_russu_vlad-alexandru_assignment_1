import { Component, OnInit } from '@angular/core';
import { CaregiverService } from '../shared/caregiver.service';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-caregiver',
  templateUrl: './caregiver.component.html',
  styleUrls: ['./caregiver.component.css']
})
export class CaregiverComponent implements OnInit {

  constructor(private caregiverService : CaregiverService) { }

  ngOnInit() {
    this.resetForm();
  }

  resetForm(form? : NgForm) {
    if(form != null)
      form.reset();
    this.caregiverService.selectedCaregiver = {
      id: null,
      name: "",
      username: "",
      password: "",
      birth_date: "",
      gender: "",
      address: "",
    }
  }

  onSubmit(form : NgForm){
    if(form.value.id == null){
      // console.log(form.value);
      this.caregiverService.postCaregiver(form.value)
      .subscribe(data => {
        this.resetForm(form);
        this.caregiverService.getAllCaregivers();
      })
    }
    else{
      this.caregiverService.putCaregiver(form.value.id, form.value)
      .subscribe(data => {
        this.resetForm(form);
        this.caregiverService.getAllCaregivers();
      })
    }
  }


}
