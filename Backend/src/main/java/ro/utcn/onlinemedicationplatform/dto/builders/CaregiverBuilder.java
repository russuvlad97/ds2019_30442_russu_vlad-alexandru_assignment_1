package ro.utcn.onlinemedicationplatform.dto.builders;

import ro.utcn.onlinemedicationplatform.dto.CaregiverDTO;
import ro.utcn.onlinemedicationplatform.entities.Caregiver;

public class CaregiverBuilder {

    public static CaregiverDTO generateDTOFromEntity(Caregiver caregiver){
        return new CaregiverDTO(
                caregiver.getId(),
                caregiver.getName(),
                caregiver.getBirth_date(),
                caregiver.getGender(),
                caregiver.getUsername(),
                caregiver.getPassword(),
                caregiver.getAddress(),
                caregiver.getPatients()
        );
    }

    public static Caregiver generateEntityFromDTO(CaregiverDTO caregiverDTO){
        return new Caregiver(
                caregiverDTO.getName(),
                caregiverDTO.getUsername(),
                caregiverDTO.getPassword(),
                caregiverDTO.getBirthDate(),
                caregiverDTO.getGender(),
                caregiverDTO.getAddress(),
                caregiverDTO.getPatients()
        );
    }

}
