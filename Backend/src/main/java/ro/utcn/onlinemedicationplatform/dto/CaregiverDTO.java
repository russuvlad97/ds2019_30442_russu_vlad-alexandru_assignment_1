package ro.utcn.onlinemedicationplatform.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ro.utcn.onlinemedicationplatform.entities.Patient;

import java.util.List;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
public class CaregiverDTO {
    private Long id;
    private String name;
    private String birthDate;
    private Character gender;
    private String username;
    private String password;
    private String address;
    private List<Patient> patients;
}
