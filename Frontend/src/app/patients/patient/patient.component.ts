import { Component, OnInit } from '@angular/core';

import { PatientService  } from '../shared/patient.service'
import { NgForm } from '@angular/forms';
import { ToastrService } from 'ngx-toastr'

@Component({
  selector: 'app-patient',
  templateUrl: './patient.component.html',
  styleUrls: ['./patient.component.css']
})
export class PatientComponent implements OnInit {

  constructor(private patientService : PatientService, private toastr : ToastrService) { }

  ngOnInit() {
    this.resetForm();
  }

  resetForm(form? : NgForm) {
    if(form != null)
      form.reset();
    this.patientService.selectedPatient = {
      id: null,
      name: "",
      username: "",
      password: "",
      birth_date: "",
      gender: "",
      address: "",
      medicalRecord: ""
    }
  }

  onSubmit(form : NgForm){
    if(form.value.id == null){
      // console.log(form.value);
      this.patientService.postPatient(form.value)
      .subscribe(data => {
        this.resetForm(form);
        this.patientService.getAllPatients();
      })
    }
    else{
      this.patientService.putPatient(form.value.id, form.value)
      .subscribe(data => {
        this.resetForm(form);
        this.patientService.getAllPatients();
      })
    }
  }

}
