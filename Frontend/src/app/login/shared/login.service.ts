import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, RequestMethod } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

import { Login } from './login.model'
import { Router } from '@angular/router';

@Injectable()

export class LoginService {

    currentLogin : Login;

    constructor(private http : Http, private router : Router) { }

    login(username : String, password : String, type : String){
        var headerOptions = new Headers({'Content-Type':'application/json'});
        var requestOptions = new RequestOptions({method : RequestMethod.Post, headers : headerOptions});
        console.log('http://localhost:8080/login/' + username + "/" + password + "/" + type);
        this.http.get('http://localhost:8080/login/' + username + "/" + password + "/" + type)
            .subscribe( data =>{
                if(type == 'doctor') this.router.navigate(['/' + type]);
                else this.router.navigate(['/' + type + '-page']);
                console.log("Should redirect to " + type)
            })
            
    }

}
