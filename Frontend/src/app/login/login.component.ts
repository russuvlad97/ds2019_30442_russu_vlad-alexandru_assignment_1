import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { LoginService } from './shared/login.service'
import { Router, ActivatedRoute } from '@angular/router';
import { Login } from './shared/login.model';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  type : String;
  returnId : number;

  constructor(
    private loginService : LoginService,
    private router : Router,
    private route : ActivatedRoute
    ) { }

  ngOnInit() {
    this.resetForm();
  }

  resetForm(form? : NgForm) {
    if(form != null)
      form.reset();
    this.loginService.currentLogin = {
      username : "",
      password : "",
      type : ""
    }
  }

  onSubmit(form : NgForm){
    this.loginService.login(form.value.username, form.value.password, this.type);
    console.log(form.value.username, form.value.password, this.type);
  }

}
