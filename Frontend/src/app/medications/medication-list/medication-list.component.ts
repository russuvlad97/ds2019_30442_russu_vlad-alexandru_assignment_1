import { Component, OnInit } from '@angular/core';
import { MedicationService } from '../shared/medication.service';
import { Medication } from '../shared/medication.model'

@Component({
  selector: 'app-medication-list',
  templateUrl: './medication-list.component.html',
  styleUrls: ['./medication-list.component.css']
})
export class MedicationListComponent implements OnInit {

  constructor(private medicationService : MedicationService) { }

  ngOnInit() {
    this.medicationService.getAllMedications();
  }

  editMedication(medication : Medication){
    this.medicationService.selectedMedication = Object.assign({}, medication);
  }

  onDelete(id : number){
    this.medicationService.deleteMedication(id)
      .subscribe(data => {
        this.medicationService.getAllMedications();
        window.location.reload();
      })
  }

}
