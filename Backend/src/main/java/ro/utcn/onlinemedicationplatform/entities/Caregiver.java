package ro.utcn.onlinemedicationplatform.entities;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ro.utcn.onlinemedicationplatform.entities.util.Type;

import javax.persistence.Entity;
import javax.persistence.OneToMany;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Entity
@Getter
@Setter
@NoArgsConstructor
public class Caregiver extends User {

    @OneToMany(mappedBy = "caregiver")
    private List<Patient> patients = new ArrayList<>();

    public Caregiver(String name, List<Patient> patients){
        super(name, Type.CAREGIVER);
        this.patients = patients;
    }

    public Caregiver(String name, String username, String password, String birthDate, Character gender, String address, List<Patient> patients) {
        super(name, username, password, Type.CAREGIVER, birthDate, gender, address);
        this.patients = patients;
    }
}
