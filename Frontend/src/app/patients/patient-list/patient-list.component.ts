import { Component, OnInit } from '@angular/core';

import { PatientService } from '../shared/patient.service'
import { Patient } from '../shared/patient.model'

@Component({
  selector: 'app-patient-list',
  templateUrl: './patient-list.component.html',
  styleUrls: ['./patient-list.component.css']
})
export class PatientListComponent implements OnInit {

  constructor(private patientService : PatientService) { }

  ngOnInit() {
    this.patientService.getAllPatients();
  }

  editPatient(patient : Patient){
    this.patientService.selectedPatient = Object.assign({}, patient);
  }

  onDelete(id : number){
     this.patientService.deletePatient(id)
      .subscribe(data => {
        this.patientService.getAllPatients();
        window.location.reload();
      });
  }

}
