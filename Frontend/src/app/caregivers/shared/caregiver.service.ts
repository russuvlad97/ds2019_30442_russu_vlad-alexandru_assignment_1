import { Injectable } from '@angular/core';
import { Http, Response, Headers, RequestOptions, RequestMethod } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/toPromise';

import {Caregiver} from './caregiver.model'

@Injectable()
export class CaregiverService {

  selectedCaregiver : Caregiver;
  caregiverList : Caregiver[];
  constructor(private http : Http) { }

  getAllCaregivers(){
    this.http.get('http://localhost:8080/doctor/caregivers')
      .map((data : Response) => {
        return data.json() as Caregiver[];
      })
      .toPromise()
      .then(x => {
        this.caregiverList = x;
      })
  }

  postCaregiver(caregiver : Caregiver){
    var body = JSON.stringify(caregiver);
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Post, headers : headerOptions});
    return this.http.post('http://localhost:8080/doctor/caregiver', body, requestOptions).map(x=>x.json());
  }

  putCaregiver(id, caregiver : Caregiver){
    var body = JSON.stringify(caregiver);
    var headerOptions = new Headers({'Content-Type':'application/json'});
    var requestOptions = new RequestOptions({method : RequestMethod.Put, headers : headerOptions});
    return this.http.put('http://localhost:8080/doctor/caregiver/' + id , body, requestOptions).map(x=>x.json());
  }

  deleteCaregiver(id : number){
    return this.http.delete('http://localhost:8080/doctor/caregiver/' + id); 
    // window.location.reload();
  }

}
