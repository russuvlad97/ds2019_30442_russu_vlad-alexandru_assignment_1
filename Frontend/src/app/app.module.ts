import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { FormsModule } from '@angular/forms'
import { HttpModule } from '@angular/http'

import { AppComponent } from './app.component';
import { DoctorComponent } from './doctor-page/doctor.component';
import { PatientsComponent } from './patients/patients.component';
import { PatientListComponent } from './patients/patient-list/patient-list.component';
import { PatientComponent } from './patients/patient/patient.component';
import { ToastrModule } from '../../node_modules/ngx-toastr';
import { LoginComponent } from './login/login.component';
import { RouterModule, Routes } from '@angular/router';
import { LoginService } from './login/shared/login.service';
import { MedicationsComponent } from './medications/medications.component';
import { MedicationComponent } from './medications/medication/medication.component';
import { MedicationListComponent } from './medications/medication-list/medication-list.component';
import { MedicationService } from './medications/shared/medication.service';
import { CaregiversComponent } from './caregivers/caregivers.component';
import { CaregiverComponent } from './caregivers/caregiver/caregiver.component';
import { CaregiverListComponent } from './caregivers/caregiver-list/caregiver-list.component';
import { CaregiverService } from './caregivers/shared/caregiver.service';
import { CaregiverPageComponent } from './caregiver-page/caregiver-page.component';
import { PatientPageComponent } from './patient-page/patient-page.component';

const routes: Routes = [
  {path: '', redirectTo: 'login', pathMatch: 'full'},
  {path: 'login', component: LoginComponent},
  {path: 'doctor', component: DoctorComponent},
  {path: 'patient-page', component: PatientPageComponent},
  {path: 'caregiver-page', component: CaregiverPageComponent}
  {path: 'patient/:id', component: PatientComponent},
];

@NgModule({
  declarations: [
    AppComponent,
    DoctorComponent,
    PatientsComponent,
    PatientComponent,
    PatientListComponent,
    LoginComponent,
    MedicationsComponent,
    MedicationComponent,
    MedicationListComponent,
    CaregiversComponent,
    CaregiverComponent,
    CaregiverListComponent,
    CaregiverPageComponent,
    PatientPageComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    ToastrModule.forRoot(),
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule],
  providers: [LoginService, MedicationService, CaregiverService],
  bootstrap: [AppComponent]
})


export class AppModule { }
